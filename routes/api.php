<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

/*Route::group(['middleware' => 'jwt-auth'], function () {
    Route::get('get_user_details', 'APIController@get_user_details');
});*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('register', 'APIController@register');
    Route::post('login', 'APIController@login');
    Route::post('logout', 'APIController@logout');

    Route::resource('users', 'UserController');
    Route::resource('drills', 'DrillController');
    Route::resource('watermarks', 'WatermarkController');
});
Route::post('upload',['as' => 'upload_file','uses' => 'DrillController@store']);

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

