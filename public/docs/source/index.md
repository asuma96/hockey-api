---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://hockey-control-api.local/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_8ae5d428da27b2b014dc767c2f19a813 -->
## Handle a registration request for the application.

first, middle, last, username, email, password

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/register" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/register",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/register`


<!-- END_8ae5d428da27b2b014dc767c2f19a813 -->
<!-- START_8c0e48cd8efa861b308fc45872ff0837 -->
## Login as user and return user data or error
email password

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/login`


<!-- END_8c0e48cd8efa861b308fc45872ff0837 -->
<!-- START_fb2ae43e2e99ff4e90f22ba03801a735 -->
## Logout
get
token

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/logout",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/logout`


<!-- END_fb2ae43e2e99ff4e90f22ba03801a735 -->
<!-- START_080f3ecebb7bcc2f93284b8f5ae1ac3b -->
## Method show list users with paginate, use param for searchable

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/users" \
-H "Accept: application/json" \
    -d "count"="82449" \
    -d "page"="82449" \
    -d "search"="est" \
    -d "searchBy"="est" \
    -d "order"="est" \
    -d "orderBy"="est" \
    -d "raw"="est" \
    -d "with"="est" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/users",
    "method": "GET",
    "data": {
        "count": 82449,
        "page": 82449,
        "search": "est",
        "searchBy": "est",
        "order": "est",
        "orderBy": "est",
        "raw": "est",
        "with": "est"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "token_invalid"
}
```

### HTTP Request
`GET api/v1/users`

`HEAD api/v1/users`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    count | integer |  optional  | 
    page | integer |  optional  | 
    search | string |  optional  | 
    searchBy | string |  optional  | 
    order | string |  optional  | Minimum: `3` Maximum: `4`
    orderBy | string |  optional  | 
    raw | string |  optional  | 
    with | string |  optional  | 

<!-- END_080f3ecebb7bcc2f93284b8f5ae1ac3b -->
<!-- START_516efe68800340987a961f28f13fffbd -->
## api/v1/users/create

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/users/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/users/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/users/create`

`HEAD api/v1/users/create`


<!-- END_516efe68800340987a961f28f13fffbd -->
<!-- START_4194ceb9a20b7f80b61d14d44df366b4 -->
## api/v1/users

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/users" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/users",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/users`


<!-- END_4194ceb9a20b7f80b61d14d44df366b4 -->
<!-- START_b4ea58dd963da91362c51d4088d0d4f4 -->
## Show user by id

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/users/{user}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "token_invalid"
}
```

### HTTP Request
`GET api/v1/users/{user}`

`HEAD api/v1/users/{user}`


<!-- END_b4ea58dd963da91362c51d4088d0d4f4 -->
<!-- START_2960955e5812bbc17bfc941ae06fad43 -->
## api/v1/users/{user}/edit

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/users/{user}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/users/{user}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/users/{user}/edit`

`HEAD api/v1/users/{user}/edit`


<!-- END_2960955e5812bbc17bfc941ae06fad43 -->
<!-- START_296fac4bf818c99f6dd42a4a0eb56b58 -->
## Update user by id

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/users/{user}" \
-H "Accept: application/json" \
    -d "email"="tromp.madalyn@example.org" \
    -d "first"="ab" \
    -d "last"="ab" \
    -d "middle"="ab" \
    -d "username"="ab" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/users/{user}",
    "method": "PUT",
    "data": {
        "email": "tromp.madalyn@example.org",
        "first": "ab",
        "last": "ab",
        "middle": "ab",
        "username": "ab"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/users/{user}`

`PATCH api/v1/users/{user}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | email |  optional  | Maximum: `255`
    first | string |  optional  | Minimum: `3` Maximum: `150`
    last | string |  optional  | Minimum: `3` Maximum: `150`
    middle | string |  optional  | Minimum: `3` Maximum: `150`
    username | string |  optional  | Minimum: `3` Maximum: `150`

<!-- END_296fac4bf818c99f6dd42a4a0eb56b58 -->
<!-- START_22354fc95c42d81a744eece68f5b9b9a -->
## Delete user by id (soft delete)

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/users/{user}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/users/{user}`


<!-- END_22354fc95c42d81a744eece68f5b9b9a -->
<!-- START_3c638c85ee52484b64b4cf8632232f63 -->
## Method show list drills with paginate, use param for searchable

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/drills" \
-H "Accept: application/json" \
    -d "count"="94477410" \
    -d "page"="94477410" \
    -d "search"="illum" \
    -d "searchBy"="illum" \
    -d "order"="illum" \
    -d "orderBy"="illum" \
    -d "raw"="illum" \
    -d "with"="illum" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/drills",
    "method": "GET",
    "data": {
        "count": 94477410,
        "page": 94477410,
        "search": "illum",
        "searchBy": "illum",
        "order": "illum",
        "orderBy": "illum",
        "raw": "illum",
        "with": "illum"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "token_invalid"
}
```

### HTTP Request
`GET api/v1/drills`

`HEAD api/v1/drills`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    count | integer |  optional  | 
    page | integer |  optional  | 
    search | string |  optional  | 
    searchBy | string |  optional  | 
    order | string |  optional  | Minimum: `3` Maximum: `4`
    orderBy | string |  optional  | 
    raw | string |  optional  | 
    with | string |  optional  | 

<!-- END_3c638c85ee52484b64b4cf8632232f63 -->
<!-- START_7f3db95d5f3a1f7a9f5feebbbdb45409 -->
## api/v1/drills/create

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/drills/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/drills/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/drills/create`

`HEAD api/v1/drills/create`


<!-- END_7f3db95d5f3a1f7a9f5feebbbdb45409 -->
<!-- START_15ee23be4f7b457edc48fa98e0c5c241 -->
## Add new drill

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/drills" \
-H "Accept: application/json" \
    -d "0"="voluptatibus" \
    -d "1"="voluptatibus" \
    -d "2"="voluptatibus" \
    -d "3"="voluptatibus" \
    -d "4"="voluptatibus" \
    -d "5"="voluptatibus" \
    -d "6"="voluptatibus" \
    -d "7"="voluptatibus" \
    -d "8"="voluptatibus" \
    -d "9"="voluptatibus" \
    -d "10"="voluptatibus" \
    -d "11"="voluptatibus" \
    -d "12"="voluptatibus" \
    -d "13"="2016-05-14" \
    -d "14"="voluptatibus" \
    -d "15"="voluptatibus" \
    -d "16"="voluptatibus" \
    -d "public"="voluptatibus" \
    -d "17"="voluptatibus" \
    -d "18"="voluptatibus" \
    -d "19"="voluptatibus" \
    -d "20"="voluptatibus" \
    -d "21"="voluptatibus" \
    -d "22"="voluptatibus" \
    -d "23"="voluptatibus" \
    -d "24"="voluptatibus" \
    -d "25"="voluptatibus" \
    -d "26"="voluptatibus" \
    -d "27"="voluptatibus" \
    -d "28"="voluptatibus" \
    -d "29"="voluptatibus" \
    -d "30"="voluptatibus" \
    -d "31"="voluptatibus" \
    -d "32"="voluptatibus" \
    -d "33"="voluptatibus" \
    -d "34"="voluptatibus" \
    -d "35"="voluptatibus" \
    -d "36"="voluptatibus" \
    -d "37"="voluptatibus" \
    -d "38"="voluptatibus" \
    -d "39"="voluptatibus" \
    -d "40"="voluptatibus" \
    -d "41"="voluptatibus" \
    -d "42"="voluptatibus" \
    -d "43"="voluptatibus" \
    -d "44"="voluptatibus" \
    -d "45"="voluptatibus" \
    -d "46"="voluptatibus" \
    -d "47"="voluptatibus" \
    -d "48"="voluptatibus" \
    -d "49"="voluptatibus" \
    -d "50"="voluptatibus" \
    -d "51"="voluptatibus" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/drills",
    "method": "POST",
    "data": {
        "0": "voluptatibus",
        "1": "voluptatibus",
        "2": "voluptatibus",
        "3": "voluptatibus",
        "4": "voluptatibus",
        "5": "voluptatibus",
        "6": "voluptatibus",
        "7": "voluptatibus",
        "8": "voluptatibus",
        "9": "voluptatibus",
        "10": "voluptatibus",
        "11": "voluptatibus",
        "12": "voluptatibus",
        "13": "2016-05-14",
        "14": "voluptatibus",
        "15": "voluptatibus",
        "16": "voluptatibus",
        "public": "voluptatibus",
        "17": "voluptatibus",
        "18": "voluptatibus",
        "19": "voluptatibus",
        "20": "voluptatibus",
        "21": "voluptatibus",
        "22": "voluptatibus",
        "23": "voluptatibus",
        "24": "voluptatibus",
        "25": "voluptatibus",
        "26": "voluptatibus",
        "27": "voluptatibus",
        "28": "voluptatibus",
        "29": "voluptatibus",
        "30": "voluptatibus",
        "31": "voluptatibus",
        "32": "voluptatibus",
        "33": "voluptatibus",
        "34": "voluptatibus",
        "35": "voluptatibus",
        "36": "voluptatibus",
        "37": "voluptatibus",
        "38": "voluptatibus",
        "39": "voluptatibus",
        "40": "voluptatibus",
        "41": "voluptatibus",
        "42": "voluptatibus",
        "43": "voluptatibus",
        "44": "voluptatibus",
        "45": "voluptatibus",
        "46": "voluptatibus",
        "47": "voluptatibus",
        "48": "voluptatibus",
        "49": "voluptatibus",
        "50": "voluptatibus",
        "51": "voluptatibus"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/drills`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    0 | string |  optional  | 
    1 | string |  optional  | 
    2 | string |  optional  | 
    3 | string |  optional  | 
    4 | string |  optional  | 
    5 | string |  optional  | 
    6 | string |  optional  | 
    7 | image |  optional  | Must be an image (jpeg, png, bmp, gif, or svg)
    8 | file |  optional  | Must be a file upload
    9 | string |  optional  | 
    10 | string |  optional  | 
    11 | string |  optional  | 
    12 | string |  optional  | 
    13 | date |  optional  | 
    14 | string |  optional  | 
    15 | string |  optional  | 
    16 | string |  optional  | 
    public | string |  optional  | 
    17 | string |  optional  | 
    18 | string |  optional  | 
    19 | string |  optional  | 
    20 | string |  optional  | 
    21 | string |  optional  | 
    22 | string |  optional  | 
    23 | string |  optional  | 
    24 | string |  optional  | 
    25 | string |  optional  | 
    26 | string |  optional  | 
    27 | string |  optional  | 
    28 | string |  optional  | 
    29 | string |  optional  | 
    30 | string |  optional  | 
    31 | string |  optional  | 
    32 | string |  optional  | 
    33 | string |  optional  | 
    34 | string |  optional  | 
    35 | string |  optional  | 
    36 | string |  optional  | 
    37 | string |  optional  | 
    38 | string |  optional  | 
    39 | string |  optional  | 
    40 | string |  optional  | 
    41 | string |  optional  | 
    42 | string |  optional  | 
    43 | string |  optional  | 
    44 | string |  optional  | 
    45 | string |  optional  | 
    46 | string |  optional  | 
    47 | string |  optional  | 
    48 | string |  optional  | 
    49 | string |  optional  | 
    50 | string |  optional  | 
    51 | string |  optional  | 

<!-- END_15ee23be4f7b457edc48fa98e0c5c241 -->
<!-- START_de00aebe6086b05c35eca3bad9a2ee78 -->
## Show drill by id

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/drills/{drill}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/drills/{drill}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/drills/{drill}`

`HEAD api/v1/drills/{drill}`


<!-- END_de00aebe6086b05c35eca3bad9a2ee78 -->
<!-- START_78f9fd0d46fb1ee0ef1bf1bb71569bdb -->
## api/v1/drills/{drill}/edit

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/drills/{drill}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/drills/{drill}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/drills/{drill}/edit`

`HEAD api/v1/drills/{drill}/edit`


<!-- END_78f9fd0d46fb1ee0ef1bf1bb71569bdb -->
<!-- START_06c1ae035a98bcea99a8dc8cbf72ae47 -->
## Update drill by id

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/drills/{drill}" \
-H "Accept: application/json" \
    -d "0"="soluta" \
    -d "1"="soluta" \
    -d "2"="soluta" \
    -d "3"="soluta" \
    -d "4"="soluta" \
    -d "5"="soluta" \
    -d "6"="soluta" \
    -d "7"="soluta" \
    -d "8"="soluta" \
    -d "9"="soluta" \
    -d "10"="soluta" \
    -d "11"="soluta" \
    -d "12"="soluta" \
    -d "13"="2002-02-18" \
    -d "14"="soluta" \
    -d "15"="soluta" \
    -d "16"="soluta" \
    -d "public"="soluta" \
    -d "17"="soluta" \
    -d "18"="soluta" \
    -d "19"="soluta" \
    -d "20"="soluta" \
    -d "21"="soluta" \
    -d "22"="soluta" \
    -d "23"="soluta" \
    -d "24"="soluta" \
    -d "25"="soluta" \
    -d "26"="soluta" \
    -d "27"="soluta" \
    -d "28"="soluta" \
    -d "29"="soluta" \
    -d "30"="soluta" \
    -d "31"="soluta" \
    -d "32"="soluta" \
    -d "33"="soluta" \
    -d "34"="soluta" \
    -d "35"="soluta" \
    -d "36"="soluta" \
    -d "37"="soluta" \
    -d "38"="soluta" \
    -d "39"="soluta" \
    -d "40"="soluta" \
    -d "41"="soluta" \
    -d "42"="soluta" \
    -d "43"="soluta" \
    -d "44"="soluta" \
    -d "45"="soluta" \
    -d "46"="soluta" \
    -d "47"="soluta" \
    -d "48"="soluta" \
    -d "49"="soluta" \
    -d "50"="soluta" \
    -d "51"="soluta" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/drills/{drill}",
    "method": "PUT",
    "data": {
        "0": "soluta",
        "1": "soluta",
        "2": "soluta",
        "3": "soluta",
        "4": "soluta",
        "5": "soluta",
        "6": "soluta",
        "7": "soluta",
        "8": "soluta",
        "9": "soluta",
        "10": "soluta",
        "11": "soluta",
        "12": "soluta",
        "13": "2002-02-18",
        "14": "soluta",
        "15": "soluta",
        "16": "soluta",
        "public": "soluta",
        "17": "soluta",
        "18": "soluta",
        "19": "soluta",
        "20": "soluta",
        "21": "soluta",
        "22": "soluta",
        "23": "soluta",
        "24": "soluta",
        "25": "soluta",
        "26": "soluta",
        "27": "soluta",
        "28": "soluta",
        "29": "soluta",
        "30": "soluta",
        "31": "soluta",
        "32": "soluta",
        "33": "soluta",
        "34": "soluta",
        "35": "soluta",
        "36": "soluta",
        "37": "soluta",
        "38": "soluta",
        "39": "soluta",
        "40": "soluta",
        "41": "soluta",
        "42": "soluta",
        "43": "soluta",
        "44": "soluta",
        "45": "soluta",
        "46": "soluta",
        "47": "soluta",
        "48": "soluta",
        "49": "soluta",
        "50": "soluta",
        "51": "soluta"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/drills/{drill}`

`PATCH api/v1/drills/{drill}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    0 | string |  optional  | 
    1 | string |  optional  | 
    2 | string |  optional  | 
    3 | string |  optional  | 
    4 | string |  optional  | 
    5 | string |  optional  | 
    6 | string |  optional  | 
    7 | image |  optional  | Must be an image (jpeg, png, bmp, gif, or svg)
    8 | file |  optional  | Must be a file upload
    9 | string |  optional  | 
    10 | string |  optional  | 
    11 | string |  optional  | 
    12 | string |  optional  | 
    13 | date |  optional  | 
    14 | string |  optional  | 
    15 | string |  optional  | 
    16 | string |  optional  | 
    public | string |  optional  | 
    17 | string |  optional  | 
    18 | string |  optional  | 
    19 | string |  optional  | 
    20 | string |  optional  | 
    21 | string |  optional  | 
    22 | string |  optional  | 
    23 | string |  optional  | 
    24 | string |  optional  | 
    25 | string |  optional  | 
    26 | string |  optional  | 
    27 | string |  optional  | 
    28 | string |  optional  | 
    29 | string |  optional  | 
    30 | string |  optional  | 
    31 | string |  optional  | 
    32 | string |  optional  | 
    33 | string |  optional  | 
    34 | string |  optional  | 
    35 | string |  optional  | 
    36 | string |  optional  | 
    37 | string |  optional  | 
    38 | string |  optional  | 
    39 | string |  optional  | 
    40 | string |  optional  | 
    41 | string |  optional  | 
    42 | string |  optional  | 
    43 | string |  optional  | 
    44 | string |  optional  | 
    45 | string |  optional  | 
    46 | string |  optional  | 
    47 | string |  optional  | 
    48 | string |  optional  | 
    49 | string |  optional  | 
    50 | string |  optional  | 
    51 | string |  optional  | 

<!-- END_06c1ae035a98bcea99a8dc8cbf72ae47 -->
<!-- START_d269b0289e40be4791e630e839a76afb -->
## Delete drill by id (soft delete)

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/drills/{drill}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/drills/{drill}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/drills/{drill}`


<!-- END_d269b0289e40be4791e630e839a76afb -->
<!-- START_4650efdac13dd77002b9aaf6f70a687a -->
## Display Watermarks by user id.

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/watermarks" \
-H "Accept: application/json" \
    -d "count"="7401" \
    -d "page"="7401" \
    -d "search"="vel" \
    -d "searchBy"="vel" \
    -d "order"="vel" \
    -d "orderBy"="vel" \
    -d "raw"="vel" \
    -d "with"="vel" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/watermarks",
    "method": "GET",
    "data": {
        "count": 7401,
        "page": 7401,
        "search": "vel",
        "searchBy": "vel",
        "order": "vel",
        "orderBy": "vel",
        "raw": "vel",
        "with": "vel"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "token_invalid"
}
```

### HTTP Request
`GET api/v1/watermarks`

`HEAD api/v1/watermarks`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    count | integer |  optional  | 
    page | integer |  optional  | 
    search | string |  optional  | 
    searchBy | string |  optional  | 
    order | string |  optional  | Minimum: `3` Maximum: `4`
    orderBy | string |  optional  | 
    raw | string |  optional  | 
    with | string |  optional  | 

<!-- END_4650efdac13dd77002b9aaf6f70a687a -->
<!-- START_70124ac2098eba5fda197cc07393870e -->
## api/v1/watermarks/create

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/watermarks/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/watermarks/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/watermarks/create`

`HEAD api/v1/watermarks/create`


<!-- END_70124ac2098eba5fda197cc07393870e -->
<!-- START_0a8db321392113e80a369963f2c9ee00 -->
## Store watermark to database
id(user), file ,customName

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/watermarks" \
-H "Accept: application/json" \
    -d "0"="quos" \
    -d "1"="quos" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/watermarks",
    "method": "POST",
    "data": [
        "quos",
        "quos"
],
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/watermarks`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    0 | string |  optional  | 
    1 | string |  optional  | 

<!-- END_0a8db321392113e80a369963f2c9ee00 -->
<!-- START_fceea3f082baa3d58658b4c7a09418d5 -->
## show watermark by id

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/watermarks/{watermark}" \
-H "Accept: application/json" \
    -d "count"="90507831" \
    -d "page"="90507831" \
    -d "search"="at" \
    -d "searchBy"="at" \
    -d "order"="at" \
    -d "orderBy"="at" \
    -d "raw"="at" \
    -d "with"="at" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/watermarks/{watermark}",
    "method": "GET",
    "data": {
        "count": 90507831,
        "page": 90507831,
        "search": "at",
        "searchBy": "at",
        "order": "at",
        "orderBy": "at",
        "raw": "at",
        "with": "at"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "token_invalid"
}
```

### HTTP Request
`GET api/v1/watermarks/{watermark}`

`HEAD api/v1/watermarks/{watermark}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    count | integer |  optional  | 
    page | integer |  optional  | 
    search | string |  optional  | 
    searchBy | string |  optional  | 
    order | string |  optional  | Minimum: `3` Maximum: `4`
    orderBy | string |  optional  | 
    raw | string |  optional  | 
    with | string |  optional  | 

<!-- END_fceea3f082baa3d58658b4c7a09418d5 -->
<!-- START_4f658331072c33d31a70d49f3a1f95a5 -->
## api/v1/watermarks/{watermark}/edit

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/watermarks/{watermark}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/watermarks/{watermark}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/watermarks/{watermark}/edit`

`HEAD api/v1/watermarks/{watermark}/edit`


<!-- END_4f658331072c33d31a70d49f3a1f95a5 -->
<!-- START_e26b0a83d767604764b6ce2ce307c4e1 -->
## api/v1/watermarks/{watermark}

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/watermarks/{watermark}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/watermarks/{watermark}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/watermarks/{watermark}`

`PATCH api/v1/watermarks/{watermark}`


<!-- END_e26b0a83d767604764b6ce2ce307c4e1 -->
<!-- START_017187c3d1c992fdd9b1dced8d506277 -->
## Delete Watermark by id

> Example request:

```bash
curl "http://hockey-control-api.local/api/v1/watermarks/{watermark}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hockey-control-api.local/api/v1/watermarks/{watermark}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/watermarks/{watermark}`


<!-- END_017187c3d1c992fdd9b1dced8d506277 -->
