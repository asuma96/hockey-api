<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWatermarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('watermarks', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('image_url');
            $table->string('image_path');
            $table->string('data');
        });
        Schema::create('user_watermark', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('watermark_id')->unsigned()->index();

        });
    }

    public function down()
    {
        Schema::drop('watermarks');
        Schema::drop('user_watermark');
    }
}
