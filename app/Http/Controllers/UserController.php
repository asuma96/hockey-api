<?php

namespace App\Http\Controllers;

use  App\Http\Controllers\Traits\SearchableTrait;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;

class UserController extends Controller
{
    use SearchableTrait;

    public function __construct()
    {
        $this->middleware('token')->only([
            'index',
            'show',
            'update',
            'destroy',
        ]);
    }

    /**
     * Method show list users with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(SearchRequest $request)
    {
        return response()->json(User::query()->searchable($request)->paginate($request->count));
    }

    /**
     * Update user by id
     *
     * @param UserRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserRequest $request, int $id)
    {
        return response()->json(User::query()->where('userid', $id)->update($request->except('api_token')));
    }

    /**
     * Delete user by id (soft delete)
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        return response()->json(User::query()->where('userid', $id)->delete());
    }

    /**
     * Show user by id
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json(User::query()->where('userid', $id)->get()->first());
    }

    public function create()
    {

    }

    public function store()
    {

    }

    public function edit($id)
    {

    }
    /* public function sendEmail($to, $subject)
     {
         $email = new \stdClass();
         $email->to = $to;
         $email->subject = $subject;
         Mail::send('emails.welcome', $email->subject, function ($message) use ($email) {
             $message->from('us@example.com', 'Laravel');
             $message->to($email->to)->cc('anton.s@cronix.ms');
         });

     }*/
    // $this->sendEmail($request['email'],['email'=> $request['email'],'password'=> $request['password']] );
}