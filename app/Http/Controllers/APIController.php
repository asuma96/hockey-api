<?php
namespace App\Http\Controllers;

use App\Models\User;
use Hash;
use Illuminate\Http\Request;
use JWTAuth;


class APIController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Handle a registration request for the application.
     *first, middle, last, username, email, password
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        if (in_array(false, [
            $request['first'],
            $request['middle'],
            $request['last'],
            $request['username'],
            $request['password'],
            $request['email']
        ])) {
            return response()->json(['result' => false, 'msg' => 'Fill all required fields please!']);
        }
        $request['password'] = Hash::make($request['password']);
        if (User::query()->where('email', $request['email'])->count() > 0) {
            return response()->json(['result' => false, 'msg' => 'This email is already in use!']);
        } elseif (User::insert([
            'first' => $request['first'],
            'middle' => $request['middle'],
            'last' => $request['last'],
            'username' => $request['username'],
            'password' => $request['password'],
            'email' => $request['email'],
            'regdate' => time(),
            'lastlogin' => time(),
            'ipaddress' => $_SERVER["REMOTE_ADDR"],
            'active' => 1,
            'verify' => 1,
            'verify_expire' => 1,
            'salt' => 1,
            'referredby' => 1,
            'hear' => 1,
            'favdrills' => 1,
            'hidedrills' => 1,
            'drillfilter' => 1,
            'step' => 1,
            'temppass' => 1,
            'regtype' => 1
        ])
        ) {
            return response()->json(['result' => true, 'msg' => 'You have successfully passed the authorization!']);
        } else {
            return response()->json(['not well']);
        }
    }

    /**
     * Login as user and return user data or error
     *email password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        if (!$token = JWTAuth::attempt($request->all())) {
            return response()->json(['ok' => false, 'message' => 'Wrong email or password.']);
        }
//        User::where('email', $request['email'])->update(['lastlogin' => time()]);
        return response()->json(['ok' => true, 'token' => $token, 'user' => JWTAuth::toUser($token)]);
    }

    /**
     * Logout
     * get
     * token
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        JWTAuth::refresh($request->token);
        return response()->json(['ok' => true, 'msg' => "You have successfully signed out"]);
    }
}