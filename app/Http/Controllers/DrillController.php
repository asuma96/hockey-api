<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Traits\SearchableTrait;
use App\Http\Requests\DrillRequest;
use App\Http\Requests\SearchRequest;
use App\Models\Animation;
use App\Models\Drill;

class DrillController extends Controller
{
    use SearchableTrait;

    public function __construct()
    {
        $this->middleware('token')->only([
            'index',
            'store',
            'update',
            'destroy',
            'show',
        ]);
    }

    /**
     * Method show list drills with paginate, use param for searchable
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(SearchRequest $request)
    {
        return response()->json(Drill::query()->where('userid', $request['id'])
            ->where(function ($query) use ($request) {
                $query->search($request->search, $request->searchBy);
            })
            ->orderBy($request->orderBy ?? 'drillid', $request->order ?? 'asc')
            ->paginate($request->count));
    }

    public static function upload(DrillRequest $request, string $folderSrc = '')
    {
        $file = $request->file('file')[0]->storePublicly($folderSrc);
        return str_replace($folderSrc . '/', '', $file);
    }

    /**
     * Add new drill
     *
     * @param DrillRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DrillRequest $request)
    {
        $img = DrillController::upload($request, 'public/drill');
        $drill = Drill::create([
            'userid' => $request['id'],
            'keypoints' => 1,
            'description' => 1,
            'comments' => 1,
            'notes' => 1,
            'vimeo' => 1,
            'thumbnail' => 1,
            'medthumbnail' => 1,
            'youtube' => 1,
            'public' => '',
            'associationid' => 1,
            'd1' => 0,
            'd2' => 0,
            'd3' => 0,
            'd4' => 0,
            'd5' => 0,
            'custcat' => 1,
            'custcat2' => 1,
            'image2' => 1.3,
            'image3' => 1,
            'image4' => 1,
            'image5' => 1,
            'pdf' => 1,
            'pdfgen' => 1,
            's3url_1' => $img,
            's3url_2' => 1,
            's3url_3' => 1,
            's3url_4' => 1,
            's3url_5' => 1,
        ]);
        return response()->json($drill);
    }

    /**
     * Add new Video oder Gif
     *
     * @param DrillRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeVideoOderGif(DrillRequest $request)
    {
        $file = DrillController::upload($request, 'public/video');
        $animation = Animation::create(['s3video' => $file, 'gif' => $request['gif'], 'name' => $request['name'], 'duration' => $request['duration']]);
        $drill = Drill::query()->where('drillid', $request['id'])->get();
        $drill[0]->animation()->attach(array(['user_id' => $drill[0]['userid'], 'name' => $drill[0]['drillname'], 'data' => $request['data'], 'animation_id' => $animation['id'], 'modified' => time()]));
        $drill[0]->save(array($animation));
        return response()->json();

    }

    /**
     * Update drill by id
     *
     * @param DrillRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DrillRequest $request, int $id)
    {
        return response()->json(Drill::query()->where('userid', $id)->update($request->except('api_token')));
    }

    /**
     * Delete drill by id (soft delete)
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy(int $id)
    {
        return response()->json(Drill::query()->where('drillid', $id)->delete());
    }

    /**
     * Show drill by id
     *
     * @param Drill $drill
     * @return \Illuminate\Http\Response
     */
    public function show(Drill $drill)
    {
        return response()->json($drill->where('drillid', $drill->drillid)->with(['animation' => function ($query) {
            $query->withPivot('name', 'data', 'modified');
        }])->first());
    }

    public function create()
    {

    }

    public function edit($id)
    {

    }
}
