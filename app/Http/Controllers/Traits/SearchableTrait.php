<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


trait SearchableTrait
{
    public function scopeSearch(Builder $q, $search, $attributes)
    {
        $attributes = is_array($attributes) ? $attributes : explode(',', $attributes);
        if (!empty($search) && !empty($attributes)) {
            foreach ($attributes as $attribute) {
                $q->orWhere($attribute, 'LIKE', "%$search%");
            }
        }

        return $q;
    }

    public function scopeInclude(Builder $q, $with)
    {
        return !empty($with) ? $q->with(explode(',', $with)) : $q;
    }

    public function scopeWithRaw(Builder $q, $raw)
    {
        return !empty($raw) ? $q->selectRaw($raw) : $q;
    }

    public function scopeSearchable(Builder $q, Request $request)
    {
        return $q->withRaw($request->raw)->include($request->with)
            ->search($request->search, $request->searchBy)
            ->orderBy($request->orderBy ?? 'userid', $request->order ?? 'asc');

    }
}
