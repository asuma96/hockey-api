<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\SearchableTrait;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\WatermarkRequest;
use App\Models\Watermark;
use Illuminate\Http\Request;

class WatermarkController extends Controller
{
    use SearchableTrait;

    public function __construct()
    {
        $this->middleware('token')->only([
            'index',
//            'store',
            'destroy',
            'show',
        ]);
    }

    /**
     * Display Watermarks by user id.
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        return response()->json(Watermark::query()->where('user_id', $request['id'])
            ->where(function ($query) use ($request) {
                $query->search($request->search, $request->searchBy);
            })
            ->orderBy($request->orderBy ?? 'id', $request->order ?? 'asc')
            ->paginate($request->count));
    }

    /**
     * show watermark by id
     *
     * @param SearchRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(SearchRequest $request, int $id)
    {
        return response()->json(Watermark::where('id', $id)->paginate($request->count));
    }

    /**
     * Delete Watermark by id
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(Watermark::query()->whereId($id)->delete());
    }

  /**
   * Store watermark to database
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
   */
    protected function store(Request $request)
    {
      $name = $request->file->getClientOriginalName();
      if(!isset(pathinfo($name)['extension'])){
        return response()->json(['ok' => false, 'msg' =>'invalid file!']);
    }
      $ext = pathinfo($name)['extension'];
      $request->file->move(storage_path() . '/watermarks', md5_file($request->file) . $ext);
        $data = [
          'customName' => $request->customName,
          'filename' =>$name,
          'size' => round($request->file->getClientSize()/1000, 1),
          'extention' => pathinfo($name)['extension'],
          'created' => time()
        ];
        Watermark::insert([
          'image_url' => 'example',
          'image_path' => 'storage/watermark',
          'data' => json_encode($data),
          'user_id' => $request->id
        ]);
      return back();
    }

    public function update()
    {

    }

    public function edit($id)
    {
        //
    }

    public function create()
    {
        //
    }
}
