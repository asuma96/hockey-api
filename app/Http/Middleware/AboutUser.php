<?php
namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AboutUser
{
    public function get_user_details(Request $request)
    {
        return response()->json(JWTAuth::toUser($request['token']));
    }
}