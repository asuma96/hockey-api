<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DrillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userid',
            'categoryid',
            'ageid',
            'drillname',
            'keypoints',
            'description',
            'notes',
            'image',
            'file',
            'featured',
            'postid',
            'dde',
            'drilllang',
            'date',
            'halfice',
            'vimeo',
            'youtube',
            'public' =>
            'associationid',
            'd1',
            'd2',
            'd3',
            'd4',
            'd5',
            'custcat',
            'diagramcount',
            'moderated	',
            'diagramtype	',
            'custcat2	',
            'complete	',
            'image2	',
            'image3	',
            'image4	',
            'image5	',
            'lastmodified',
            'pdf	',
            'pdfgen',
            'teamshare',
            'use_count',
            'is_copy	',
            'comments',
            'rating	',
            'thumbnail',
            'medthumbnail',
            's3url_1	',
            's3url_2',
            's3url_3	',
            's3url_4	',
            's3url_5	',
            'vert	',
            'has_animation',
            'paid	',
            'has_gif	',
            'deleted	',
        ];
    }
}
