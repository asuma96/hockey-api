<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'email|max:255|unique:users',
            'first' => 'string|min:3|max:150',
            'last' => 'string|min:3|max:150',
            'middle' => 'string|min:3|max:150',
            'username' => 'string|min:3|max:150',
        ];
    }
}
