<?php

namespace App\Models;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Watermark extends Model
{
    use SearchableTrait, Notifiable;
    protected $table = 'watermarks';
    protected $primaryKey = 'id';
  protected $fillable = [
        'user_id',
        'image_url',
        'image_path',
        'data',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
