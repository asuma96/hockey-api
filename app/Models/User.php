<?php

namespace App\Models;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable, SearchableTrait;

    protected $table = 'hs_users';
    protected $primaryKey = 'userid';
    protected $fillable = [
        'first',
        'middle',
        'last',
        'username',
        'email',
        'regdate',
        'lastlogin',
        'lastlogin_s',
        'active',
        'ipaddress',
        'verify',
        'verify_expire',
        'salt',
        'referredby',
        'hear',
        'favdrills',
        'hidedrills',
        'drillfilter',
        'step',
        'temppass',
        'regtype',
        'password',
    ];

    protected $hidden = [

    ];
    public function drill()
    {
        return $this->hasMany(Drill::class,'userid');
    }

    public function watermark()
    {
        return $this->hasMany(Watermark::class);
    }
    public function animation()
    {
        return $this->belongsToMany(Animation::class,'diagrammer_drills','user_id','animation_id');
    }

}