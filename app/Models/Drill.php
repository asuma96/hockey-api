<?php

namespace App\Models;

use App\Http\Controllers\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Drill extends Model
{
    use SearchableTrait;
    protected $primaryKey = 'drillid';
    protected $table = 'drills';
    protected $fillable = [
        'userid',
        'categoryid',
        'ageid',
        'drillname',
        'keypoints',
        'description',
        'notes',
        'image',
        'file',
        'featured',
        'postid',
        'dde',
        'drilllang',
        'date',
        'halfice',
        'vimeo',
        'youtube',
        'public	',
        'associationid',
        'd1',
        'd2',
        'd3',
        'd4',
        'd5',
        'custcat',
        'diagramcount',
        'moderated',
        'diagramtype',
        'custcat2',
        'complete',
        'image2',
        'image3',
        'image4',
        'image5',
        'lastmodified',
        'pdf',
        'pdfgen',
        'teamshare',
        'use_count',
        'is_copy',
        'comments',
        'rating',
        'thumbnail',
        'medthumbnail',
        's3url_1',
        's3url_2',
        's3url_3',
        's3url_4',
        's3url_5',
        'vert',
        'has_animation',
        'paid',
        'has_gif',
        'deleted',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function animation()
    {
        return $this->belongsToMany(Animation::class, 'diagrammer_drills', 'drillid', 'animation_id');
    }

}
