<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Traits\SearchableTrait;

class Animation extends Model
{
    use  SearchableTrait;

    protected $table = 'animations';
    protected $fillable = [
        'name',
        'duration',
        's3video',
        'ready',
        'gif',
    ];
    public function user()
    {
        return $this->belongsToMany(User::class,'diagrammer_drills');
    }
    public function drill()
    {
        return $this->belongsToMany(Drill::class,'diagrammer_drills','drillid','animation_id');
    }
}
